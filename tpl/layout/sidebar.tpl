[{foreach from=$oxidBlock_sidebar item="_block"}]
    [{$_block}]
    [{/foreach}]
<div class="d-none d-lg-block">
[{block name="sidebar"}]
        [{block name="sidebar_categoriestree"}]
        [{*if $oView->getClassName() == 'alist'*}]
        <div class="box card categorytree">
            <section>
                <div class="page-header h3">
                    <div class="pull-right d-block d-sm-inline-block">
                        <i class="fa fa-caret-down toggleTree"></i>
                    </div>
                    [{oxmultilang ident="MANUFACTUREN"}]
                </div>
                [{oxid_include_widget cl="oxwCategoryTree" cnid=$oView->getCategoryId() sWidgetType="header" deepLevel=1 noscript=1 nocookie=1}]
            </section>
        </div>
        [{*/if*}]
        [{/block}]
    [{/block}]

</div>